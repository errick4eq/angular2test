import { Component, OnInit } from '@angular/core';
import { UserService } from './../../services/user.service';

import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
  public selectedUser: any = {};

  constructor(
    private userService: UserService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  getUser(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    return this.userService.getUsers().find(user => {
      return user.id === id;
    }) || {};
  }

}
