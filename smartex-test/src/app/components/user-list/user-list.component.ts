import { Component, OnInit, Output } from '@angular/core';
import { UserService } from './../../services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  users: any = [];
  selectedUser: any = [];

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
  }

  public selectUser(u) {
    this.userService.selectUser(u);
    this.selectedUser = u;
  }

}
