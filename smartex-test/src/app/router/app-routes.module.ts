import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UserInfoComponent } from './../components/user-info/user-info.component';

const routes: Routes = [

  { path: ':id', pathMatch: 'full', component: UserInfoComponent },
  { path: '', pathMatch: 'full', component: UserInfoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutesModule { }
