import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppRoutesModule } from './app-routes.module';

@NgModule({
  imports: [
    CommonModule,
    AppRoutesModule
  ],
  declarations: []
})
export class AppRouterModule { }
