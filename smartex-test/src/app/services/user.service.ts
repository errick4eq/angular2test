import { Injectable } from '@angular/core';
import { HttpParams, HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public selectedUserSubject: BehaviorSubject<any> = new BehaviorSubject({});
  public readonly selectedUser: Observable<any> = this.selectedUserSubject.asObservable();

  users: any = [];
  currentUrl = 'http://jsonplaceholder.typicode.com/users';

constructor(
  private http: HttpClient
) {
  this.http.get(this.currentUrl).toPromise().then(response => {
    this.users = response;
  });
 }

public getUsers() {
  return this.users;
}

public selectUser(param) {
  this.selectedUserSubject.next(param);
}

}
